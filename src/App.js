import React from 'react';
import axios from 'axios';
import './App.css';
import NewsItem from './NewsItem.component';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listNews: [],
    };
  }

  componentDidMount() {
    console.info('haloo');
    // call axios
    axios({
      method: 'GET',
      url:
        'http://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=8e953c0bb0884d7088c0a0730f55e5f8',
    })
      .then((res) => {
        // when success
        console.info('articles', res.data.articles);
        this.setState({ listNews: res.data.articles });
      })
      .catch((err) => {
        // when error
      });
  }

  render() {
    return (

      <div className="container">
        <div className="main-title">NEWS FOR YOU</div>
        <div>
          {this.state.listNews.map((news, index) => (
            <NewsItem
              index={index}
              image={news.urlToImage}
              title={news.title}
              name={news.author}
              date={news.publishedAt}
              description={news.description}
            />
          ))}
          
        </div>
      </div>
    );
  }
}
